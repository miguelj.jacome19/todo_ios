//
// Created by Miguel Jimenez on 11/14/17.
// Copyright (c) 2017 Bruno Philipe. All rights reserved.
//

import UIKit
import FBSDKLoginKit

class LoginViewController: UIViewController, FBSDKLoginButtonDelegate{

    let loginButton = FBSDKLoginButton()
    @IBOutlet var tokenLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loginButton.delegate = self
        self.addConstraints()

    }

    func addConstraints(){
        loginButton.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(loginButton)

        let widthConstraint = loginButton.widthAnchor.constraint(equalToConstant: 250)
        let heightConstraint = loginButton.heightAnchor.constraint(equalToConstant: 100)
        let centerXConstraint = loginButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor)
        let centerYConstraint = loginButton.centerYAnchor.constraint(equalTo: self.view.centerYAnchor)

        NSLayoutConstraint.activate([widthConstraint, heightConstraint, centerXConstraint, centerYConstraint])
    }

    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
            tokenLabel.text = ""
    }

    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if error != nil{
                tokenLabel.text = "Login Error"
        }else{
            if(result.token != nil){
                tokenLabel.text = "\(result.token!)"
            }
        }
    }
}
